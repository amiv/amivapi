
db.createUser(
    {
        user: "amivapi",
        pwd: "amivapi",
        roles: [
            {
                role: "readWrite",
                db: "amivapi"
            }
        ]
    }
);



let userId = db.users.insertOne({
    nethz: 'admin',
    password: '$pbkdf2-sha256$5$OqfUmtNaq5UyRohxDuGckw$9H/UL5N5dA7JmUq7ohRPfmJ84OUnpRKjTgsMeuFilXM',
    email: "admin@example.com",
    membership: "regular",
    firstname: "ad",
    lastname: "min",
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

let groupId = db.groups.insertOne({
    name: 'admin',
    permissions: {
        apikeys: "readwrite",
        users: "readwrite",
        sessions: "readwrite",
        events: "readwrite",
        eventsignups: "readwrite",
        groups: "readwrite",
        groupmemberships: "readwrite",
        joboffers: "readwrite",
        beverages: "read",
        studydocuments: "readwrite",
        oauthclients: "readwrite",
        blacklist: "readwrite",
    },
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

db.groupmemberships.insertOne({
    user: userId,
    group: groupId,
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
})

db.oauthclients.insertOne({
    client_id: "Local Tool",
    redirect_uri: "http://localhost:9000/oauthcallback",
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
});

let manyUserId = []

for(let i = 0; i < 10; i++){
    manyUserId[i] = db.users.insertOne({
        nethz: 'test' + i.toString(),
        password: '$pbkdf2-sha256$1000$MSbEOKf0njOGkHLOOad0rg$j4GZopKcBA0TyqWZWBCPxDoVOeHOH8RtMlP9UgRu1uY',
        email: "hans" + i.toString() + ".muster@example.com",
        membership: "regular",
        department: "itet",
        firstname: "Hans" + i.toString(),
        lastname: "Muster",
        legi: null,
        phone: null,
        rfid: null,
        send_newsletter: false,
        _etag: "27f987fd9dd45d491e5aea3e27730israndom",
    }).insertedId;
}

let manyGroupId = []

manyGroupId[0] = db.groups.insertOne({
    name: 'test0',
    permissions: {
        apikeys: "read",
        users: "readwrite",
        sessions: "readwrite",
        events: "readwrite",
        eventsignups: "readwrite",
        groups: "readwrite",
        groupmemberships: "readwrite",
        joboffers: "readwrite",
        beverages: "read",
        studydocuments: "readwrite",
        oauthclients: "read",
    },
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

manyGroupId[1] = db.groups.insertOne({
    name: 'test1',
    permissions: {
        apikeys: "read",
        users: "read",
        sessions: "readwrite",
        events: "read",
        eventsignups: "read",
        groups: "readwrite",
        groupmemberships: "readwrite",
        joboffers: "readwrite",
        beverages: "read",
        studydocuments: "readwrite",
        oauthclients: "read",
    },
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

manyGroupId[2] = db.groups.insertOne({
    name: 'test2',
    permissions: {
        apikeys: "read",
        users: "read",
        sessions: "read",
        events: "read",
        eventsignups: "read",
        groups: "read",
        groupmemberships: "read",
        joboffers: "readwrite",
        beverages: "read",
        studydocuments: "readwrite",
        oauthclients: "read",
    },
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

for(let i = 0; i < 10; i++){
    db.groupmemberships.insertOne({
        user: manyUserId[i],
        group: manyGroupId[i%3],
        _etag: "27f987fd9dd45d491e5aea3e27730israndom",
    })
}

db.groupmemberships.insertOne({
    user: manyUserId[1],
    group: manyGroupId[0],
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
})
db.groupmemberships.insertOne({
    user: manyUserId[2],
    group: manyGroupId[0],
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
})

let manyEventsId = []

manyEventsId[0] = db.events.insertOne({
    title_en: 'Ev En0',
    catchphrase_en: 'Super good',
    description_en: 'past event',
    title_de: 'Ev De0',
    catchphrase_de: 'mega gut',
    description_de: 'vergangenes Event',
    time_start: ISODate('1024-12-20T11:12:00.000Z'),
    time_end: ISODate('1024-12-20T16:17:00.000Z'),
    price: 500,
    time_advertising_start: ISODate('1024-11-14T11:12:00.000Z'),
    time_advertising_end: ISODate('1024-11-20T16:17:00.000Z'),
    type: 'internal',
    additional_fields: null,
    priority: 1,
    location: null,
    img_infoscreen: null,
    img_poster: null,
    img_thumbnail: null,
    show_announce: false,
    show_infoscreen: false,
    show_website: false,
    spots: 10,
    time_register_start: ISODate('1024-11-14T11:12:00.000Z'),
    time_register_end: ISODate('1024-11-18T11:12:00.000Z'),
    time_deregister_end: ISODate('1024-11-19T11:12:00.000Z'),
    signup_additional_info_en: null,
    signup_additional_info_de: null,
    allow_email_signup: false,
    selection_strategy: 'fcfs',
    external_registration: null,
    moderator: userId,
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

manyEventsId[1] = db.events.insertOne({
    title_en: 'Ev En1',
    catchphrase_en: 'Super good',
    description_en: 'currently signup possible',
    title_de: 'Ev De1',
    catchphrase_de: 'mega gut',
    description_de: 'Singup ist moeglich',
    time_start: ISODate('2124-12-20T11:12:00.000Z'),
    time_end: ISODate('2124-12-20T16:17:00.000Z'),
    price: 500,
    time_advertising_start: ISODate('2024-11-14T11:12:00.000Z'),
    time_advertising_end: ISODate('2124-11-20T16:17:00.000Z'),
    type: 'internal',
    additional_fields: null,
    priority: 1,
    location: null,
    img_infoscreen: null,
    img_poster: null,
    img_thumbnail: null,
    show_announce: false,
    show_infoscreen: false,
    show_website: false,
    spots: 4,
    time_register_start: ISODate('2024-11-14T11:12:00.000Z'),
    time_register_end: ISODate('2124-11-18T11:12:00.000Z'),
    time_deregister_end: ISODate('2124-11-19T11:12:00.000Z'),
    signup_additional_info_en: null,
    signup_additional_info_de: null,
    allow_email_signup: false,
    selection_strategy: 'fcfs',
    external_registration: null,
    moderator: userId,
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

manyEventsId[2] = db.events.insertOne({
    title_en: 'Ev En2',
    catchphrase_en: 'Super good',
    description_en: 'event in the future',
    title_de: 'Ev De2',
    catchphrase_de: 'mega gut',
    description_de: 'Event in der Zukunft',
    time_start: ISODate('3099-12-20T11:12:00.000Z'),
    time_end: ISODate('3099-12-20T16:17:00.000Z'),
    price: 500,
    time_advertising_start: ISODate('3099-11-14T11:12:00.000Z'),
    time_advertising_end: ISODate('3099-11-20T16:17:00.000Z'),
    type: 'internal',
    additional_fields: null,
    priority: 1,
    location: null,
    img_infoscreen: null,
    img_poster: null,
    img_thumbnail: null,
    show_announce: false,
    show_infoscreen: false,
    show_website: false,
    spots: 0,
    time_register_start: ISODate('3099-11-14T11:12:00.000Z'),
    time_register_end: ISODate('3099-11-18T11:12:00.000Z'),
    time_deregister_end: ISODate('3099-11-19T11:12:00.000Z'),
    signup_additional_info_en: null,
    signup_additional_info_de: null,
    allow_email_signup: false,
    selection_strategy: 'fcfs',
    external_registration: null,
    moderator: userId,
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

for(let i = 0; i < 10; i++){
    db.eventsignups.insertOne({
        accepted: true,
        additional_fields: null,
        checked_in: null,
        confirmed: true,
        user: manyUserId[i],
        event: manyEventsId[i%3],
        _etag: "27f987fd9dd45d491e5aea3e27730israndom",
    })
}

let blacklistUserId = db.users.insertOne({
    nethz: 'blackl',
    password: '$pbkdf2-sha256$1000$MSbEOKf0njOGkHLOOad0rg$j4GZopKcBA0TyqWZWBCPxDoVOeHOH8RtMlP9UgRu1uY',
    email: "black.listuser@ethz.ch",
    membership: "regular",
    departement: "itet",
    firstname: "User",
    lastname: "Blacklist",
    legi: null,
    phone: null,
    rfid: null,
    send_newsletter: false,
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
}).insertedId;

db.blacklist.insertOne({
    user: blacklistUserId,
    end_time: null,
    price: 500,
    reason: "This user is the dummy for the blacklist",
    start_time: ISODate("2024-10-11T00:00:00Z"),
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
})

db.joboffers.insertOne({
    company: "Dummy AG",
    description_de: "Die Dummy AG",
    description_en: "The Dummy AG",
    logo: { // does somthing strange, but it works...
                file: "/media/5aed79e484e60300014e7550",
                name: "vseth.png",
                content_type: "image/png",
                length: 3073,
                upload_date: ISODate("2020-10-11T00:00:00Z")
            },
    pdf: null,
    show_website: false,
    time_end: "2028-10-10T12:00:00Z",
    title_de: "Job in der Dummy AG",
    title_en: "Job in the Dummy AG",
    _etag: "27f987fd9dd45d491e5aea3e27730israndom",
})
