""" Config file used for tox tests.
It is called by the utils.py setUp function. """
from passlib.context import CryptContext
import logging

# Do not change this has to be the same as in tests/utils.py
MONGO_HOST = 'mongodb'
MONGO_PORT = 27017
MONGO_DBNAME = 'test_amivapi'
MONGO_USERNAME = 'test_user'
MONGO_PASSWORD = 'test_pw'

ROOT_PASSWORD = 'root'

API_MAIL = 'api@test.ch'
SMTP_SERVER = ''
TESTING = True
DEBUG = True   # This makes eve's error messages more helpful
LDAP_USERNAME = None  # LDAP test require special treatment
LDAP_PASSWORD = None  # LDAP test require special treatment
SENTRY_DSN = None
SENTRY_ENVIRONMENT = None
PASSWORD_CONTEXT = CryptContext(
    schemes=["pbkdf2_sha256"],
    pbkdf2_sha256__default_rounds=10,
    # min_rounds is used to determine if a hash needs to be upgraded
    pbkdf2_sha256__min_rounds=8,
)

LOG_LEVEL = logging.DEBUG
