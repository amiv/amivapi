import argparse
from pymongo import MongoClient


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='AMIVAPI Gender Remover 9000',
        description='removes gender from all users in amivapi'
    )
    parser.add_argument('-H', '--host')
    parser.add_argument('-P', '--port')
    parser.add_argument('-u', '--user')
    parser.add_argument('-p', '--password')
    parser.add_argument('-s', '--suffix')
    parser.add_argument('--undo', action='store_true')
    args = parser.parse_args()

    if args.suffix:
        dbname = "amivapi_{}".format(args.suffix)
    else:
        dbname = "amivapi"

    client = MongoClient(
        host=args.host,
        port=int(args.port),
        username=args.user,
        password=args.password,
        authSource=dbname)

    db = client[dbname]

    if not args.undo:
        db.users.update_many({}, {'$unset': {'gender': ""}})
        print("bye bye gender!!!")
    else:
        db.users.update_many({}, {'$set': {'gender': "female"}})
        print("amiv frauenquote fixed!!")
